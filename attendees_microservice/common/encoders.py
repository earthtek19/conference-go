from .json import ModelEncoder
from attendees.models import ConferenceVO
from attendees.models import Attendee


# conferenceVO encoders
class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


# attendee encoders
class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
    ]

    def get_extra_data(self, o):
        return {"conference": o.conference.name}


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]
